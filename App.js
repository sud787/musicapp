/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import 'react-native-gesture-handler'
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import { Icon } from 'react-native-elements';
import MusicianProfile from './musicianProfile'
import Home from './home';
import VenueProfile from './venueProfile'
import {StackNavigator} from './stackNavigation'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  ImageBackground,
} from 'react-native';

export const AppNavigator = createBottomTabNavigator({
  Home:
  {
    screen: StackNavigator,
    navigationOptions: {
      title: "Home",
      tabBarLabel: "Home",
      tabBarIcon: <Icon size={22} color="grey" name="home" type='material-community' />,
      tabBarOnPress: ({ navigation, defaultHandler }) => {
        //  navigation.popToTop();
        defaultHandler();
      }
    }
  },
  Venue:
  {
    screen: VenueProfile,
    navigationOptions: {
      title: "Venue",
      tabBarLabel: "Venue",
      tabBarIcon: <Icon size={22} color="grey" name="music-note" type='material-community' />,
      tabBarOnPress: ({ navigation, defaultHandler }) => {
        //  navigation.popToTop();
        defaultHandler();
      }
    }
  },

}, {
  initialRouteName: 'Home',
  tabBarOptions: {
    activeTintColor: 'red',
    inactiveTintColor: 'black',
    showLabel: false,
    tabStyle: {
      backgroundColor: 'black',


    },
    style: {

      borderTopColor: 'black',
    }
  },
}


);
export default createAppContainer(AppNavigator);


