/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    ImageBackground,
} from 'react-native';

import { Card, Badge, Avatar, Button } from 'react-native-elements'

export default class MusicianProfile extends React.Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView style={{ backgroundColor: 'black', }} showsVerticalScrollIndicator={false}>
                    <View style={{ flex: 1, }}>
                        <ImageBackground source={require('./musicianCover.jpeg')} style={{ width: '100%', height: '90%' }}>
                            <Card containerStyle={{ borderRadius: 5, padding: 20, marginTop: 250, }} >

                                <View style={{ flexDirection: 'row', }}>
                                    <View style={{ flex: 0.5, justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                                        <Text style={{ color: 'rgba(0, 0, 0, 0.43)' }}>The Local Metro</Text>
                                        <Text style={{ fontSize: 18, lineHeight: 21, marginTop: 10 }}>Sudhanshu Kumar</Text>

                                        <Badge
                                            size="30"
                                            value="TIP"
                                            textStyle={{
                                                fontSize: 12,
                                                fontWeight: 'bold',
                                                lineHeight: 14,
                                                color: 'white'
                                            }}
                                            badgeStyle={{ backgroundColor: 'black', width: 50 }}
                                            containerStyle={{ marginTop: 10 }}
                                        />
                                    </View>
                                    <View style={{ flex: 0.5, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                        <Avatar
                                            overlayContainerStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.43)' }}
                                            rounded
                                            icon={{ name: 'user', type: 'font-awesome', }}
                                            size='large'
                                        />
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 30 }}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 24, lineHeight: 28 }}>6.5K</Text>
                                        <Text style={{ fontSize: 15, lineHeight: 14, color: '#AAAAAA', marginTop: 10 }}>Followers</Text>
                                    </View>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 24, lineHeight: 28 }}>140</Text>
                                        <Text style={{ fontSize: 15, lineHeight: 14, color: '#AAAAAA', marginTop: 10 }}>Tips</Text>
                                    </View>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 24, lineHeight: 28 }}>12</Text>
                                        <Text style={{ fontSize: 15, lineHeight: 14, color: '#AAAAAA', marginTop: 10 }}>Events</Text>
                                    </View>
                                </View>
                            </Card>
                        </ImageBackground>

                        <Button title='Follow'
                            containerStyle={{ paddingLeft: 30, paddingRight: 30, marginTop: 15 }}
                            titleStyle={{ fontSize: 18, lineHeight: 16 }}
                            buttonStyle={{ backgroundColor: 'black', borderWidth: 0.5, borderColor: '#FFFFFF', borderRadius: 65 }}
                        />
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', }}>
                        <Card
                            containerStyle={{ backgroundColor: 'black', borderWidth: 0.5, borderRadius: 5 }}
                            //    title='Live'
                            image={require('./musicEventLive.jpeg')}
                            featuredTitle='Chain Kuli k Main Kuli'
                            featuredSubtitle='Bollywood Music Night'
                        >
                            <View style={{ alignItems: 'center' }}>
                                <Text style={{ color: 'white' }}>Live</Text>
                            </View>
                        </Card>
                    </View>

                    <View style={{ paddingLeft: 18, paddingRight: 18, marginTop: 30 }}>
                        <Text style={{ color: 'white', fontSize: 16, lineHeight: 18 }}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequ.
        </Text>
                    </View>
                    <View style={{ marginTop: 50 }}>
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                        >
                            <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                                <Text style={{ color: 'white', marginLeft: 15 }}>Today,25th Aug </Text>
                                <Card

                                    titleStyle={{ fontSize: 12, padding: 0, backgroundColor: 'black' }}
                                    containerStyle={{
                                        backgroundColor: '#2D2D2D',
                                        borderColor: '#2D2D2D',
                                        borderWidth: 0.5, borderRadius: 5, height: 181, width: 142,
                                    }}


                                >

                                </Card>
                            </View>
                            <View>
                                <Text style={{ color: 'white' }}> </Text>
                                <Card
                                    containerStyle={{
                                        backgroundColor: '#2D2D2D',
                                        borderColor: '#2D2D2D',
                                        borderWidth: 0.5, borderRadius: 5, height: 181, width: 142,
                                    }}

                                >
                                    <View style={{ alignItems: 'center' }}>

                                    </View>
                                </Card>
                            </View>
                            <View>
                                <Text style={{ color: 'white' }}>Tom,26th Aug </Text>
                                <Card
                                    containerStyle={{
                                        backgroundColor: '#2D2D2D',
                                        borderColor: '#2D2D2D',
                                        borderWidth: 0.5, borderRadius: 5, height: 181, width: 142,
                                    }}

                                >
                                    <View style={{ alignItems: 'center' }}>

                                    </View>
                                </Card>
                            </View>
                            <View>
                                <Text style={{ color: 'white' }}></Text>

                                <Card
                                    containerStyle={{
                                        backgroundColor: '#2D2D2D',
                                        borderColor: '#2D2D2D',
                                        borderWidth: 0.5, borderRadius: 5, height: 181, width: 142,
                                    }}

                                >
                                    <View style={{ alignItems: 'center' }}>

                                    </View>
                                </Card>
                            </View>
                            <View>
                                <Text style={{ color: 'white' }}></Text>

                                <Card
                                    containerStyle={{
                                        backgroundColor: '#2D2D2D',
                                        borderColor: '#2D2D2D',
                                        borderWidth: 0.5, borderRadius: 5, height: 181, width: 142,
                                    }}

                                >
                                    <View style={{ alignItems: 'center' }}>

                                    </View>
                                </Card>
                            </View>
                        </ScrollView>
                    </View>

                    <View style={{ paddingLeft: 15, marginTop: 50, paddingRight: 15 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                            <Text style={{ color: 'white', fontSize: 24, lineHeight: 28 }}>Music Spotlight</Text>
                            <Text style={{ color: 'rgba(255, 255, 255, 0.44)', fontSize: 12, lineHeight: 12 }}>View All</Text>
                        </View>
                        <View style={{ marginTop: 30, flexDirection: 'row', flex: 1 }}>
                            <Avatar
                                size={130}
                                icon={{ name: 'user', type: 'font-awesome' }}
                                overlayContainerStyle={{ backgroundColor: '#2D2D2D', borderRadius: 10, }}
                            />
                            <View style={{ marginLeft: 10, justifyContent: 'space-between', alignItems: 'flex-start', flex: 1 }}>
                                <Text style={{ color: 'rgba(255, 255, 255, 0.38)', fontSize: 14, lineHeight: 14 }}>NEW RELEASE</Text>
                                <Text style={{ color: 'white', fontSize: 24, lineHeight: 28 }}>The Southern kung -fu</Text>
                                <Text style={{ color: 'rgba(255, 255, 255, 0.38)', fontSize: 14, lineHeight: 14 }}>7 SONGS</Text>
                            </View>
                            <Badge
                                //  size="1300"
                                value="Stream"
                                textStyle={{
                                    fontSize: 12,
                                    paddingLeft: 5,
                                    lineHeight: 14,
                                    color: 'black',
                                    //margin:5
                                }}
                                badgeStyle={{ backgroundColor: '#C4C4C4', width: 50 }}
                                containerStyle={{ marginTop: 10 }}
                            />
                        </View>

                    </View>
                    <View style={{ paddingLeft: 15, marginTop: 30, paddingRight: 15, height: 300 }}>
                        <Text style={{ color: 'rgba(255, 255, 255, 0.38);' }}>POPULAR SONGS</Text>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    size='large'
                                    icon={{ name: 'user', type: 'font-awesome' }}
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D', borderRadius: 10, }}
                                />
                                <View style={{ marginLeft: 15, justifyContent: 'flex-start', alignItems: 'flex-start', flex: 1, }}>
                                    <Text style={{ color: 'white', fontSize: 14, lineHeight: 14 }}>NEW RELEASE</Text>
                                    <Text style={{ color: 'rgba(255, 255, 255, 0.44)', fontSize: 14, lineHeight: 14, }}>Saurabh Shetty</Text>
                                </View>
                                <Badge
                                    //  size="1300"
                                    value="Stream"
                                    textStyle={{
                                        fontSize: 12,
                                        paddingLeft: 5,
                                        lineHeight: 14,
                                        color: 'black',
                                        //margin:5
                                    }}
                                    badgeStyle={{ backgroundColor: '#C4C4C4', width: 50 }}
                                    containerStyle={{ marginTop: 10 }}
                                />
                            </View>
                            <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    size='large'
                                    icon={{ name: 'user', type: 'font-awesome' }}
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D', borderRadius: 10, }}
                                />
                                <View style={{ marginLeft: 15, justifyContent: 'flex-start', alignItems: 'flex-start', flex: 1, }}>
                                    <Text style={{ color: 'white', fontSize: 14, lineHeight: 14 }}>NEW RELEASE</Text>
                                    <Text style={{ color: 'rgba(255, 255, 255, 0.44)', fontSize: 14, lineHeight: 14, }}>Saurabh Shetty</Text>
                                </View>
                                <Badge
                                    //  size="1300"
                                    value="Stream"
                                    textStyle={{
                                        fontSize: 12,
                                        paddingLeft: 5,
                                        lineHeight: 14,
                                        color: 'black',
                                        //margin:5
                                    }}
                                    badgeStyle={{ backgroundColor: '#C4C4C4', width: 50 }}
                                    containerStyle={{ marginTop: 10 }}
                                />
                            </View>
                            <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    size='large'
                                    icon={{ name: 'user', type: 'font-awesome' }}
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D', borderRadius: 10, }}
                                />
                                <View style={{ marginLeft: 15, justifyContent: 'flex-start', alignItems: 'flex-start', flex: 1, }}>
                                    <Text style={{ color: 'white', fontSize: 14, lineHeight: 14 }}>NEW RELEASE</Text>
                                    <Text style={{ color: 'rgba(255, 255, 255, 0.44)', fontSize: 14, lineHeight: 14, }}>Saurabh Shetty</Text>
                                </View>
                                <Badge
                                    //  size="1300"
                                    value="Stream"
                                    textStyle={{
                                        fontSize: 12,
                                        paddingLeft: 5,
                                        lineHeight: 14,
                                        color: 'black',
                                        //margin:5
                                    }}
                                    badgeStyle={{ backgroundColor: '#C4C4C4', width: 50 }}
                                    containerStyle={{ marginTop: 10 }}
                                />
                            </View>
                            <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    size='large'
                                    icon={{ name: 'user', type: 'font-awesome' }}
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D', borderRadius: 10, }}
                                />
                                <View style={{ marginLeft: 15, justifyContent: 'flex-start', alignItems: 'flex-start', flex: 1, }}>
                                    <Text style={{ color: 'white', fontSize: 14, lineHeight: 14 }}>NEW RELEASE</Text>
                                    <Text style={{ color: 'rgba(255, 255, 255, 0.44)', fontSize: 14, lineHeight: 14, }}>Saurabh Shetty</Text>
                                </View>
                                <Badge
                                    //  size="1300"
                                    value="Stream"
                                    textStyle={{
                                        fontSize: 12,
                                        paddingLeft: 5,
                                        lineHeight: 14,
                                        color: 'black',
                                        //margin:5
                                    }}
                                    badgeStyle={{ backgroundColor: '#C4C4C4', width: 50 }}
                                    containerStyle={{ marginTop: 10 }}
                                />
                            </View>
                            <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    size='large'
                                    icon={{ name: 'user', type: 'font-awesome' }}
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D', borderRadius: 10, }}
                                />
                                <View style={{ marginLeft: 15, justifyContent: 'flex-start', alignItems: 'flex-start', flex: 1, }}>
                                    <Text style={{ color: 'white', fontSize: 14, lineHeight: 14 }}>NEW RELEASE</Text>
                                    <Text style={{ color: 'rgba(255, 255, 255, 0.44)', fontSize: 14, lineHeight: 14, }}>Saurabh Shetty</Text>
                                </View>
                                <Badge
                                    //  size="1300"
                                    value="Stream"
                                    textStyle={{
                                        fontSize: 12,
                                        paddingLeft: 5,
                                        lineHeight: 14,
                                        color: 'black',
                                        //margin:5
                                    }}
                                    badgeStyle={{ backgroundColor: '#C4C4C4', width: 50 }}
                                    containerStyle={{ marginTop: 10 }}
                                />
                            </View>
                        </ScrollView>

                    </View>

                    <View style={{ marginTop: 50 }}>
                        <Text style={{ color: 'white', marginLeft: 15 }}>Hightlights from </Text>
                        <Text style={{ color: 'white', marginLeft: 15, marginTop: 5, fontSize: 26, lineHeight: 28 }}>Mixtape No.10 </Text>
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            contentContainerStyle={{ marginTop: 30 }}
                        >

                            <Card

                                titleStyle={{ fontSize: 12, padding: 0, backgroundColor: 'black' }}
                                containerStyle={{
                                    backgroundColor: '#2D2D2D',
                                    borderColor: '#2D2D2D',
                                    borderWidth: 0.5, borderRadius: 3, height: 146, width: 220,
                                }}


                            >

                            </Card>

                            <Card
                                containerStyle={{
                                    backgroundColor: '#2D2D2D',
                                    borderColor: '#2D2D2D',
                                    borderWidth: 0.5, borderRadius: 3, height: 146, width: 220,
                                }}

                            >
                                <View style={{ alignItems: 'center' }}>

                                </View>
                            </Card>

                            <Card
                                containerStyle={{
                                    backgroundColor: '#2D2D2D',
                                    borderColor: '#2D2D2D',
                                    borderWidth: 0.5, borderRadius: 3, height: 146, width: 220,
                                }}

                            >
                                <View style={{ alignItems: 'center' }}>

                                </View>
                            </Card>


                            <Card
                                containerStyle={{
                                    backgroundColor: '#2D2D2D',
                                    borderColor: '#2D2D2D',
                                    borderWidth: 0.5, borderRadius: 3, height: 146, width: 220,
                                }}

                            >
                                <View style={{ alignItems: 'center' }}>

                                </View>
                            </Card>


                            <Card
                                containerStyle={{
                                    backgroundColor: '#2D2D2D',
                                    borderColor: '#2D2D2D',
                                    borderWidth: 0.5, borderRadius: 3, height: 146, width: 220,
                                }}

                            >
                                <View style={{ alignItems: 'center' }}>

                                </View>
                            </Card>

                        </ScrollView>
                    </View>
                    <View style={{ marginTop: 30 }}>
                        <Text style={{ marginLeft: 15, color: 'rgba(255, 255, 255, 0.38);', fontSize: 14, lineHeight: 14 }}>MORE HIGHLIGHTS</Text>
                        <Card
                            containerStyle={{
                                backgroundColor: '#2D2D2D',
                                borderColor: '#2D2D2D',
                                borderWidth: 0.5, borderRadius: 3, height: 251,
                                flex: 1,
                                justifyContent: 'flex-end'
                            }}

                        >
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ color: 'white' }}>240 Photos and Videos</Text>
                                <Text style={{ color: 'rgba(255, 255, 255, 0.44)' }}>View All</Text>
                            </View>



                        </Card>
                    </View>
                    <View style={{ marginTop: 50 }}>
                        <Text style={{ color: 'white', marginLeft: 15, marginTop: 5, fontSize: 26, lineHeight: 28 }}>Explore Artists  </Text>
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            contentContainerStyle={{ marginTop: 30 }}
                        >
                            <View style={{ marginLeft: 15, marginTop: 5, justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D' }}
                                    rounded
                                    size={88}
                                />
                                <Text style={{ color: 'white' }}>Saurabh</Text>
                                <Text style={{ color: 'white' }}>Kuhad </Text>
                            </View>
                            <View style={{ marginLeft: 15, marginTop: 5, justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D' }}
                                    rounded
                                    size={88}
                                />
                                <Text style={{ color: 'white' }}>Saurabh </Text>
                                <Text style={{ color: 'white' }}>Kuhad </Text>
                            </View>
                            <View style={{ marginLeft: 15, marginTop: 5, justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D' }}
                                    rounded
                                    size={88}
                                />
                                <Text style={{ color: 'white' }}>Saurabh </Text>
                                <Text style={{ color: 'white' }}>Kuhad </Text>
                            </View>
                            <View style={{ marginLeft: 15, marginTop: 5, justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D' }}
                                    rounded
                                    size={88}
                                />
                                <Text style={{ color: 'white' }}>Prateek </Text>
                                <Text style={{ color: 'white' }}>Kuhad </Text>

                            </View>
                            <View style={{ marginLeft: 15, marginTop: 5, justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D' }}
                                    rounded
                                    size={88}
                                />
                                <Text style={{ color: 'white' }}>Saurabh</Text>
                                <Text style={{ color: 'white' }}>Shetty </Text>
                            </View>


                        </ScrollView>
                    </View>

                </ScrollView>
            </SafeAreaView>
        );
    };
}


