import React from 'react';
import 'react-native-gesture-handler'
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import { Icon } from 'react-native-elements';
import MusicianProfile from './musicianProfile'
import Home from './home';
import VenueProfile from './venueProfile';
import UpcomingEventDetails from './upcomingEventDetails'
export const StackNavigator = createStackNavigator({

    Home: {
        // `Home` is a React component that will be the main content of the screen.
        screen: Home,
        navigationOptions: ({ navigation }) => ({
            header: null,
        })
    },
    MusicianProfile: {
        screen: MusicianProfile,
        navigationOptions: ({ navigation }) => ({
            //  header: null,
        })
    },
    UpcomingEventDetails: {

        screen: UpcomingEventDetails,
        navigationOptions: ({ navigation }) => ({
            //  header: null,
        })
    }
}, {
    initialRouteName: 'Home'
})