/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';

import { Card, Badge, Avatar, Button } from 'react-native-elements'

export default class UpcomingEventDetails extends React.Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView style={{ backgroundColor: 'black', }} showsVerticalScrollIndicator={false}>
                    <View style={{ marginTop: 50, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 16, color: 'rgba(255, 255, 255, 0.28)' }}>Scrolled through all the latest content?</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 20, paddingLeft: 40, paddingRight: 40 }}>
                            <Avatar

                                overlayContainerStyle={{ backgroundColor: 'white', borderRadius: 10 }}
                                title="Explore Music"
                                titleStyle={{ color: 'black', fontSize: 13, paddingTop: 50, lineHeight: 12 }}
                                containerStyle={{ height: 80, width: 98 }}
                            />
                            <Avatar
                                overlayContainerStyle={{ backgroundColor: 'white', borderRadius: 10 }}
                                containerStyle={{ height: 80, width: 98 }}
                                title="Explore Events"
                                titleStyle={{ color: 'black', fontSize: 13, paddingTop: 50, lineHeight: 12 }}
                            //  size={88}
                            />
                            <Avatar
                                overlayContainerStyle={{ backgroundColor: 'white', borderRadius: 10 }}
                                title="Explore Artists"

                                titleStyle={{ color: 'black', fontSize: 13, paddingTop: 50, lineHeight: 12 }}
                                containerStyle={{ height: 80, width: 98 }}
                            />
                        </View>
                    </View>
                    <View style={{ marginTop: 50 }}>
                        <Text style={{ color: 'white', marginLeft: 15 }}>Hightlights from </Text>
                        <Text style={{ color: 'white', marginLeft: 15, marginTop: 5, fontSize: 26, lineHeight: 28 }}>Mixtape No.10 </Text>
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            contentContainerStyle={{ marginTop: 30 }}
                        >

                            <Card

                                titleStyle={{ fontSize: 12, padding: 0, backgroundColor: 'black' }}
                                containerStyle={{
                                    backgroundColor: '#2D2D2D',
                                    borderColor: '#2D2D2D',
                                    borderWidth: 0.5, borderRadius: 3, height: 146, width: 220,
                                }}


                            >

                            </Card>

                            <Card
                                containerStyle={{
                                    backgroundColor: '#2D2D2D',
                                    borderColor: '#2D2D2D',
                                    borderWidth: 0.5, borderRadius: 3, height: 146, width: 220,
                                }}

                            >
                                <View style={{ alignItems: 'center' }}>

                                </View>
                            </Card>

                            <Card
                                containerStyle={{
                                    backgroundColor: '#2D2D2D',
                                    borderColor: '#2D2D2D',
                                    borderWidth: 0.5, borderRadius: 3, height: 146, width: 220,
                                }}

                            >
                                <View style={{ alignItems: 'center' }}>

                                </View>
                            </Card>


                            <Card
                                containerStyle={{
                                    backgroundColor: '#2D2D2D',
                                    borderColor: '#2D2D2D',
                                    borderWidth: 0.5, borderRadius: 3, height: 146, width: 220,
                                }}

                            >
                                <View style={{ alignItems: 'center' }}>

                                </View>
                            </Card>


                            <Card
                                containerStyle={{
                                    backgroundColor: '#2D2D2D',
                                    borderColor: '#2D2D2D',
                                    borderWidth: 0.5, borderRadius: 3, height: 146, width: 220,
                                }}
                            >
                                <View style={{ alignItems: 'center' }}>

                                </View>
                            </Card>

                        </ScrollView>
                    </View>
                    <View style={{ marginTop: 50 }}>
                        <Text style={{ color: 'white', marginLeft: 15, marginTop: 5, fontSize: 26, lineHeight: 28 }}>Artists Spotlight </Text>
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            contentContainerStyle={{ marginTop: 30 }}
                        >
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('MusicianProfile')}
                                activeOpacity={0.9}
                                style={{ marginLeft: 15, marginTop: 5, justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D' }}
                                    rounded
                                    size={88}

                                />
                                <Text style={{ color: 'white' }}>Saurabh</Text>
                                <Text style={{ color: 'white' }}>Kuhad </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.9}
                                onPress={() => this.props.navigation.navigate('MusicianProfile')}
                                style={{ marginLeft: 15, marginTop: 5, justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D' }}
                                    rounded
                                    size={88}
                                />
                                <Text style={{ color: 'white' }}>Saurabh </Text>
                                <Text style={{ color: 'white' }}>Kuhad </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.9}
                                onPress={() => this.props.navigation.navigate('MusicianProfile')}
                                style={{ marginLeft: 15, marginTop: 5, justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D' }}
                                    rounded
                                    size={88}
                                />
                                <Text style={{ color: 'white' }}>Saurabh </Text>
                                <Text style={{ color: 'white' }}>Kuhad </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.9}
                                onPress={() => this.props.navigation.navigate('MusicianProfile')}
                                style={{ marginLeft: 15, marginTop: 5, justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D' }}
                                    rounded
                                    size={88}
                                />
                                <Text style={{ color: 'white' }}>Prateek </Text>
                                <Text style={{ color: 'white' }}>Kuhad </Text>

                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.9}
                                onPress={() => this.props.navigation.navigate('MusicianProfile')}
                                style={{ marginLeft: 15, marginTop: 5, justifyContent: 'center', alignItems: 'center' }}>

                                <Avatar
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D' }}
                                    rounded
                                    size={88}
                                />
                                <Text style={{ color: 'white' }}>Saurabh</Text>
                                <Text style={{ color: 'white' }}>Shetty </Text>
                            </TouchableOpacity>


                        </ScrollView>
                    </View>
                    <View style={{ paddingLeft: 15, marginTop: 30, paddingRight: 15, height: 300 }}>
                        <Text style={{ color: 'white', marginTop: 5, fontSize: 26, lineHeight: 28 }}>Music Spotlight </Text>

                        <ScrollView showsVerticalScrollIndicator={false}>
                            <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    size='large'
                                    icon={{ name: 'user', type: 'font-awesome' }}
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D', borderRadius: 10, }}
                                />
                                <View style={{ marginLeft: 15, justifyContent: 'flex-start', alignItems: 'flex-start', flex: 1, }}>
                                    <Text style={{ color: 'white', fontSize: 14, lineHeight: 14 }}>NEW RELEASE</Text>
                                    <Text style={{ color: 'rgba(255, 255, 255, 0.44)', fontSize: 14, lineHeight: 14, }}>Saurabh Shetty</Text>
                                </View>
                                <Badge
                                    //  size="1300"
                                    value="Stream"
                                    textStyle={{
                                        fontSize: 12,
                                        paddingLeft: 5,
                                        lineHeight: 14,
                                        color: 'black',
                                        //margin:5
                                    }}
                                    badgeStyle={{ backgroundColor: '#C4C4C4', width: 50 }}
                                    containerStyle={{ marginTop: 10 }}
                                />
                            </View>
                            <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    size='large'
                                    icon={{ name: 'user', type: 'font-awesome' }}
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D', borderRadius: 10, }}
                                />
                                <View style={{ marginLeft: 15, justifyContent: 'flex-start', alignItems: 'flex-start', flex: 1, }}>
                                    <Text style={{ color: 'white', fontSize: 14, lineHeight: 14 }}>NEW RELEASE</Text>
                                    <Text style={{ color: 'rgba(255, 255, 255, 0.44)', fontSize: 14, lineHeight: 14, }}>Saurabh Shetty</Text>
                                </View>
                                <Badge
                                    //  size="1300"
                                    value="Stream"
                                    textStyle={{
                                        fontSize: 12,
                                        paddingLeft: 5,
                                        lineHeight: 14,
                                        color: 'black',
                                        //margin:5
                                    }}
                                    badgeStyle={{ backgroundColor: '#C4C4C4', width: 50 }}
                                    containerStyle={{ marginTop: 10 }}
                                />
                            </View>
                            <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    size='large'
                                    icon={{ name: 'user', type: 'font-awesome' }}
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D', borderRadius: 10, }}
                                />
                                <View style={{ marginLeft: 15, justifyContent: 'flex-start', alignItems: 'flex-start', flex: 1, }}>
                                    <Text style={{ color: 'white', fontSize: 14, lineHeight: 14 }}>NEW RELEASE</Text>
                                    <Text style={{ color: 'rgba(255, 255, 255, 0.44)', fontSize: 14, lineHeight: 14, }}>Saurabh Shetty</Text>
                                </View>
                                <Badge
                                    //  size="1300"
                                    value="Stream"
                                    textStyle={{
                                        fontSize: 12,
                                        paddingLeft: 5,
                                        lineHeight: 14,
                                        color: 'black',
                                        //margin:5
                                    }}
                                    badgeStyle={{ backgroundColor: '#C4C4C4', width: 50 }}
                                    containerStyle={{ marginTop: 10 }}
                                />
                            </View>
                            <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    size='large'
                                    icon={{ name: 'user', type: 'font-awesome' }}
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D', borderRadius: 10, }}
                                />
                                <View style={{ marginLeft: 15, justifyContent: 'flex-start', alignItems: 'flex-start', flex: 1, }}>
                                    <Text style={{ color: 'white', fontSize: 14, lineHeight: 14 }}>NEW RELEASE</Text>
                                    <Text style={{ color: 'rgba(255, 255, 255, 0.44)', fontSize: 14, lineHeight: 14, }}>Saurabh Shetty</Text>
                                </View>
                                <Badge
                                    //  size="1300"
                                    value="Stream"
                                    textStyle={{
                                        fontSize: 12,
                                        paddingLeft: 5,
                                        lineHeight: 14,
                                        color: 'black',
                                        //margin:5
                                    }}
                                    badgeStyle={{ backgroundColor: '#C4C4C4', width: 50 }}
                                    containerStyle={{ marginTop: 10 }}
                                />
                            </View>
                            <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Avatar
                                    size='large'
                                    icon={{ name: 'user', type: 'font-awesome' }}
                                    overlayContainerStyle={{ backgroundColor: '#2D2D2D', borderRadius: 10, }}
                                />
                                <View style={{ marginLeft: 15, justifyContent: 'flex-start', alignItems: 'flex-start', flex: 1, }}>
                                    <Text style={{ color: 'white', fontSize: 14, lineHeight: 14 }}>NEW RELEASE</Text>
                                    <Text style={{ color: 'rgba(255, 255, 255, 0.44)', fontSize: 14, lineHeight: 14, }}>Saurabh Shetty</Text>
                                </View>
                                <Badge
                                    //  size="1300"
                                    value="Stream"
                                    textStyle={{
                                        fontSize: 12,
                                        paddingLeft: 5,
                                        lineHeight: 14,
                                        color: 'black',
                                        //margin:5
                                    }}
                                    badgeStyle={{ backgroundColor: '#C4C4C4', width: 50 }}
                                    containerStyle={{ marginTop: 10 }}
                                />
                            </View>
                        </ScrollView>

                    </View>
                    <View style={{ marginTop: 50 }}>
                        <Text style={{ color: 'white', marginLeft: 15, marginTop: 5, fontSize: 26, lineHeight: 28 }}>Events In Pune </Text>

                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            style={{ marginTop: 30 }}
                        >

                            <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                                <Text style={{ color: 'white', marginLeft: 15 }}>Today,25th Aug </Text>
                                <Card

                                    titleStyle={{ fontSize: 12, padding: 0, backgroundColor: 'black' }}
                                    containerStyle={{
                                        backgroundColor: '#2D2D2D',
                                        borderColor: '#2D2D2D',
                                        borderWidth: 0.5, borderRadius: 5, height: 181, width: 142,
                                    }}


                                >

                                </Card>
                            </View>
                            <View>
                                <Text style={{ color: 'white' }}> </Text>
                                <Card
                                    containerStyle={{
                                        backgroundColor: '#2D2D2D',
                                        borderColor: '#2D2D2D',
                                        borderWidth: 0.5, borderRadius: 5, height: 181, width: 142,
                                    }}

                                >
                                    <View style={{ alignItems: 'center' }}>

                                    </View>
                                </Card>
                            </View>
                            <View>
                                <Text style={{ color: 'white' }}>Tom,26th Aug </Text>
                                <Card
                                    containerStyle={{
                                        backgroundColor: '#2D2D2D',
                                        borderColor: '#2D2D2D',
                                        borderWidth: 0.5, borderRadius: 5, height: 181, width: 142,
                                    }}

                                >
                                    <View style={{ alignItems: 'center' }}>

                                    </View>
                                </Card>
                            </View>
                            <View>
                                <Text style={{ color: 'white' }}></Text>

                                <Card
                                    containerStyle={{
                                        backgroundColor: '#2D2D2D',
                                        borderColor: '#2D2D2D',
                                        borderWidth: 0.5, borderRadius: 5, height: 181, width: 142,
                                    }}

                                >
                                    <View style={{ alignItems: 'center' }}>

                                    </View>
                                </Card>
                            </View>
                            <View>
                                <Text style={{ color: 'white' }}></Text>

                                <Card
                                    containerStyle={{
                                        backgroundColor: '#2D2D2D',
                                        borderColor: '#2D2D2D',
                                        borderWidth: 0.5, borderRadius: 5, height: 181, width: 142,
                                    }}

                                >
                                    <View style={{ alignItems: 'center' }}>

                                    </View>
                                </Card>
                            </View>
                        </ScrollView>
                    </View>




                </ScrollView>
            </SafeAreaView>
        );
    };
}


