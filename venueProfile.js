/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    ImageBackground,
} from 'react-native';

import { Card, Badge, Avatar, Image, Icon } from 'react-native-elements'
import { black } from 'ansi-colors';


export default class VenueProfile extends React.Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView style={{ backgroundColor: 'black', }} showsVerticalScrollIndicator={false}>
                    <ImageBackground source={require('./venueCover.jpeg')}
                        style={{ width: '100%', height: 300 }}
                        resizeMode={'cover'}>
                        <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center', }}>
                            <Image
                                style={{ width: 170, height: 100, }}
                                source={require('./venueLogo.png')} />
                            <Text style={{ color: 'white', fontSize: 16, marginTop: 10 }}>Pub / Continental / Bakery</Text>


                        </View>
                    </ImageBackground>

                    <View style={{ flex: 1, justifyContent: 'center', }}>
                        <Card
                            containerStyle={{ backgroundColor: 'black', borderWidth: 0.5, borderRadius: 5 }}
                            //    title='Live'
                            image={require('./musicEventLive.jpeg')}
                            featuredTitle='Chain Kuli k Main Kuli'
                            featuredSubtitle='Bollywood Music Night'
                        >
                            <View style={{ alignItems: 'center' }}>
                                <Text style={{ color: 'white' }}>Live</Text>
                            </View>
                        </Card>
                    </View>


                    <View style={{ marginTop: 50 }}>
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                        >
                            <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                                <Text style={{ color: 'white', marginLeft: 15 }}>Today,25th Aug </Text>
                                <Card

                                    titleStyle={{ fontSize: 12, padding: 0, backgroundColor: 'black' }}
                                    containerStyle={{
                                        backgroundColor: '#2D2D2D',
                                        borderColor: '#2D2D2D',
                                        borderWidth: 0.5, borderRadius: 5, height: 181, width: 142,
                                    }}


                                >

                                </Card>
                            </View>
                            <View>
                                <Text style={{ color: 'white' }}> </Text>
                                <Card
                                    containerStyle={{
                                        backgroundColor: '#2D2D2D',
                                        borderColor: '#2D2D2D',
                                        borderWidth: 0.5, borderRadius: 5, height: 181, width: 142,
                                    }}

                                >
                                    <View style={{ alignItems: 'center' }}>

                                    </View>
                                </Card>
                            </View>
                            <View>
                                <Text style={{ color: 'white' }}>Tom,26th Aug </Text>
                                <Card
                                    containerStyle={{
                                        backgroundColor: '#2D2D2D',
                                        borderColor: '#2D2D2D',
                                        borderWidth: 0.5, borderRadius: 5, height: 181, width: 142,
                                    }}

                                >
                                    <View style={{ alignItems: 'center' }}>

                                    </View>
                                </Card>
                            </View>
                            <View>
                                <Text style={{ color: 'white' }}></Text>

                                <Card
                                    containerStyle={{
                                        backgroundColor: '#2D2D2D',
                                        borderColor: '#2D2D2D',
                                        borderWidth: 0.5, borderRadius: 5, height: 181, width: 142,
                                    }}

                                >
                                    <View style={{ alignItems: 'center' }}>

                                    </View>
                                </Card>
                            </View>
                            <View>
                                <Text style={{ color: 'white' }}></Text>

                                <Card
                                    containerStyle={{
                                        backgroundColor: '#2D2D2D',
                                        borderColor: '#2D2D2D',
                                        borderWidth: 0.5, borderRadius: 5, height: 181, width: 142,
                                    }}

                                >
                                    <View style={{ alignItems: 'center' }}>

                                    </View>
                                </Card>
                            </View>
                        </ScrollView>
                    </View>

                    <ScrollView
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{ marginTop: 30 }}
                    >
                        <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start', flexDirection: 'row' }}>
                            <Card
                                containerStyle={{
                                    backgroundColor: 'black',
                                    borderWidth: 0.5,
                                    borderColor: 'black',
                                    borderRadius: 5,
                                    height: 181, width: 300,
                                }}
                                //    title='Live'
                                image={require('./offers.png')}
                                featuredTitle='50% off on every third drink*'
                                featuredSubtitle='Offer Applicable on only IMFL '
                            >
                            </Card>
                            <Card
                                containerStyle={{
                                    backgroundColor: 'black',
                                    borderWidth: 0.5,
                                    borderColor: 'black',
                                    borderRadius: 5,
                                    height: 181, width: 300,
                                }}
                                //    title='Live'
                                image={require('./offers.png')}
                                featuredTitle='50% off on every third drink*'
                                featuredSubtitle='Offer Applicable on only IMFL '
                            >
                            </Card>
                            <Card
                                containerStyle={{
                                    backgroundColor: 'black',
                                    borderWidth: 0.5,
                                    borderColor: 'black',
                                    borderRadius: 5,
                                    height: 181, width: 300,
                                }}
                                //    title='Live'
                                image={require('./offers.png')}
                                featuredTitle='50% off on every third drink*'
                                featuredSubtitle='Offer Applicable on only IMFL '
                            >
                            </Card>
                            <Card
                                containerStyle={{
                                    backgroundColor: 'black',
                                    borderWidth: 0.5,
                                    borderColor: 'black',
                                    borderRadius: 5,
                                    height: 181, width: 300,
                                }}
                                //    title='Live'
                                image={require('./offers.png')}
                                featuredTitle='50% off on every third drink*'
                                featuredSubtitle='Offer Applicable on only IMFL '
                            >
                            </Card>
                        </View>
                    </ScrollView>
                    <View style={{ marginTop: 50 }}>

                        <Text style={{ color: 'white', marginLeft: 15, marginTop: 5, fontSize: 26, lineHeight: 28 }}>Hightlights </Text>

                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            contentContainerStyle={{ marginTop: 30 }}

                        >
                            <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start', flexDirection: 'row' }}>
                                <Card
                                    containerStyle={{
                                        backgroundColor: 'black',
                                        borderColor: 'black',
                                        height: 181, width: 300,
                                    }}
                                    //    title='Live'
                                    image={require('./highlights.png')}


                                >
                                </Card>
                                <Card
                                    containerStyle={{
                                        backgroundColor: 'black',

                                        borderColor: 'black',
                                        borderRadius: 5,
                                        height: 181, width: 300,
                                    }}
                                    //    title='Live'
                                    image={require('./highlights.png')}

                                >
                                </Card>
                                <Card
                                    containerStyle={{
                                        backgroundColor: 'black',
                                        borderWidth: 0.5,
                                        borderColor: 'black',
                                        borderRadius: 5,
                                        height: 181, width: 300,
                                    }}
                                    //    title='Live'
                                    image={require('./highlights.png')}

                                >
                                </Card>
                                <Card
                                    containerStyle={{
                                        backgroundColor: 'black',
                                        borderWidth: 0.5,
                                        borderColor: 'black',
                                        borderRadius: 5,
                                        height: 181, width: 300,
                                    }}
                                    //    title='Live'
                                    image={require('./highlights.png')}

                                >
                                </Card>
                            </View>
                        </ScrollView>
                    </View>
                    <View style={{ paddingLeft: 18, paddingRight: 18, marginTop: 30 }}>
                        <Text style={{ color: 'rgba(255, 255, 255, 0.51)', fontSize: 16, lineHeight: 16 }}>ABOUT</Text>
                        <Text style={{ color: 'white', fontSize: 16, lineHeight: 18, marginTop: 30 }}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequ.
                        </Text>
                    </View>
                    <View style={{ paddingLeft: 18, paddingRight: 18, marginTop: 30 }}>
                        <Text style={{ color: 'rgba(255, 255, 255, 0.51)', fontSize: 16, lineHeight: 16 }}>MORE INFO</Text>
                        <View style={{ marginTop: 20 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                <View style={{ flex: 0.45, flexDirection: 'row', alignItems: 'center', backgroundColor: '#0C0C0C', borderRadius: 5, height: 35, padding: 10 }}>
                                    <Badge badgeStyle={{ backgroundColor: 'white' }} />
                                    <Text style={{ color: 'white', marginLeft: 5 }}>Random info</Text>

                                </View>
                                <View style={{ flex: 0.45, flexDirection: 'row', alignItems: 'center', backgroundColor: '#0C0C0C', borderRadius: 5, height: 35, padding: 10 }}>
                                    <Badge badgeStyle={{ backgroundColor: 'white' }} />
                                    <Text style={{ color: 'white', marginLeft: 5 }}>Random info</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                <View style={{ flex: 0.45, flexDirection: 'row', alignItems: 'center', backgroundColor: '#0C0C0C', borderRadius: 5, height: 35, padding: 10 }}>
                                    <Badge badgeStyle={{ backgroundColor: 'white' }} />
                                    <Text style={{ color: 'white', marginLeft: 5 }}>Random info</Text>

                                </View>
                                <View style={{ flex: 0.45, flexDirection: 'row', alignItems: 'center', backgroundColor: '#0C0C0C', borderRadius: 5, height: 35, padding: 10 }}>
                                    <Badge badgeStyle={{ backgroundColor: 'white' }} />
                                    <Text style={{ color: 'white', marginLeft: 5 }}>Random info</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                <View style={{ flex: 0.45, flexDirection: 'row', alignItems: 'center', backgroundColor: '#0C0C0C', borderRadius: 5, height: 35, padding: 10 }}>
                                    <Badge badgeStyle={{ backgroundColor: 'white' }} />
                                    <Text style={{ color: 'white', marginLeft: 5 }}>Random info</Text>

                                </View>
                                <View style={{ flex: 0.45, flexDirection: 'row', alignItems: 'center', backgroundColor: '#0C0C0C', borderRadius: 5, height: 35, padding: 10 }}>
                                    <Badge badgeStyle={{ backgroundColor: 'white' }} />
                                    <Text style={{ color: 'white', marginLeft: 5 }}>Random info</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                <View style={{ flex: 0.45, flexDirection: 'row', alignItems: 'center', backgroundColor: '#0C0C0C', borderRadius: 5, height: 35, padding: 10 }}>
                                    <Badge badgeStyle={{ backgroundColor: 'white' }} />
                                    <Text style={{ color: 'white', marginLeft: 5 }}>Random info</Text>

                                </View>
                                <View style={{ flex: 0.45, flexDirection: 'row', alignItems: 'center', backgroundColor: '#0C0C0C', borderRadius: 5, height: 35, padding: 10 }}>
                                    <Badge badgeStyle={{ backgroundColor: 'white' }} />
                                    <Text style={{ color: 'white', marginLeft: 5 }}>Random info</Text>
                                </View>
                            </View>

                        </View>
                    </View>
                    <View>
                        <View style={{ paddingLeft: 18, paddingRight: 28, marginTop: 30, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{ color: 'white', fontSize: 26, lineHeight: 28 }}>Reviews</Text>
                            <View style={{ flex: 0.2, flexDirection: 'row', backgroundColor: 'white', borderRadius: 5, justifyContent: 'center', alignItems: 'center' }}>
                                <Icon
                                    name='star'
                                    type='material-community'
                                    color='black'
                                    size={13}
                                />

                                <Text>4.3</Text>
                            </View>

                        </View>
                        <Card
                            containerStyle={{
                                backgroundColor: '#0C0C0C',
                                borderWidth: 0.5,
                                borderColor: 'black',
                                borderRadius: 5,
                                margin: 20
                            }}

                        >
                            <View style={{ flexDirection: 'row', justifyContent: 'center', paddingTop: 20 }}>
                                <Icon
                                    name='star'
                                    type='material-community'
                                    color='white'
                                    size={16}
                                />
                                <Icon
                                    name='star'
                                    type='material-community'
                                    color='white'
                                    size={16}
                                />
                                <Icon
                                    name='star'
                                    type='material-community'
                                    color='white'
                                    size={16}
                                />
                                <Icon
                                    name='star-outline'
                                    type='material-community'
                                    color='white'
                                    size={16}
                                />
                                <Icon
                                    name='star-outline'
                                    type='material-community'
                                    color='white'
                                    size={16}
                                />
                            </View>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 26, color: 'white' }}>Mereko idhar roti chahiye!</Text>
                                <Text style={{ fontSize: 13, color: 'rgba(255, 255, 255, 0.39)' }}>HIMESH RESHAMIYA</Text>
                            </View>
                        </Card>
                    </View>
                    <View style={{ paddingLeft: 18, paddingRight: 18, marginTop: 30 }}>
                        <Text style={{ fontSize: 26, color: 'white' }}>Venues around you</Text>
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            contentContainerStyle={{ marginTop: 30 }}
                        >
                            <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start', flexDirection: 'row' }}>
                                <Card
                                    containerStyle={{
                                        backgroundColor: 'black',
                                        borderWidth: 0.5,
                                        borderColor: 'black',
                                        borderRadius: 5,

                                    }}
                                    //    title='Live'
                                    image={require('./venueLogo.png')}
                                    imageStyle={{
                                        height: 42, width: 72,
                                    }}
                                >
                                    <View style={{ 
                                        flexDirection: 'row',
                                         backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon
                                            name='star'
                                            type='material-community'
                                            color='black'
                                            size={11}
                                        />
                                        <Text>4.3</Text>
                                    </View>

                                </Card>
                                <Card
                                    containerStyle={{
                                        backgroundColor: 'black',
                                        borderWidth: 0.5,
                                        borderColor: 'black',
                                        borderRadius: 5,

                                    }}
                                    //    title='Live'
                                    image={require('./venueLogo.png')}
                                    imageStyle={{
                                        height: 42, width: 72,
                                    }}
                                >
                                     <View style={{ 
                                        flexDirection: 'row',
                                         backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon
                                            name='star'
                                            type='material-community'
                                            color='black'
                                            size={11}
                                        />
                                        <Text>4.3</Text>
                                    </View>
                                </Card>
                                <Card
                                    containerStyle={{
                                        backgroundColor: 'black',
                                        borderWidth: 0.5,
                                        borderColor: 'black',
                                        borderRadius: 5,

                                    }}
                                    //    title='Live'
                                    image={require('./venueLogo.png')}
                                    imageStyle={{
                                        height: 42, width: 72,
                                    }}
                                >
                                     <View style={{ 
                                        flexDirection: 'row',
                                         backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon
                                            name='star'
                                            type='material-community'
                                            color='black'
                                            size={11}
                                        />
                                        <Text>4.3</Text>
                                    </View>
                                </Card>

                                <Card
                                    containerStyle={{
                                        backgroundColor: 'black',
                                        borderWidth: 0.5,
                                        borderColor: 'black',
                                        borderRadius: 5,

                                    }}
                                    //    title='Live'
                                    image={require('./venueLogo.png')}
                                    imageStyle={{
                                        height: 42, width: 72,
                                    }}
                                >
                                     <View style={{ 
                                        flexDirection: 'row',
                                         backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon
                                            name='star'
                                            type='material-community'
                                            color='black'
                                            size={11}
                                        />
                                        <Text>4.3</Text>
                                    </View>
                                </Card>
                                <Card
                                    containerStyle={{
                                        backgroundColor: 'black',
                                        borderWidth: 0.5,
                                        borderColor: 'black',
                                        borderRadius: 5,

                                    }}
                                    //    title='Live'
                                    image={require('./venueLogo.png')}
                                    imageStyle={{
                                        height: 42, width: 72,
                                    }}
                                >
                                     <View style={{ 
                                        flexDirection: 'row',
                                         backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon
                                            name='star'
                                            type='material-community'
                                            color='black'
                                            size={11}
                                        />
                                        <Text>4.3</Text>
                                    </View>
                                </Card>
                            </View>
                        </ScrollView>
                    </View>
                </ScrollView>

            </SafeAreaView>
        );
    };
}


